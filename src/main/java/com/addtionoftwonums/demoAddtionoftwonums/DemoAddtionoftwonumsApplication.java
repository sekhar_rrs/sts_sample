package com.addtionoftwonums.demoAddtionoftwonums;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoAddtionoftwonumsApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoAddtionoftwonumsApplication.class, args);
	}
}
