package com.addtionoftwonums.demoAddtionoftwonums;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("test")
public class AddController {

    @RequestMapping(value = "/add")
    public String add(@RequestParam("t1") int i, @RequestParam("t2") int j){
        return (i + j +"");
    }
    @RequestMapping(value = "")
    public String index(){
        return "index";
    }

}
